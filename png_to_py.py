from PIL import Image
from copy import deepcopy
import argparse


parser = argparse.ArgumentParser(description='Converts images to card10 parsable images')
parser.add_argument('--ignore_alpha', '-a', dest='alpha', action='store_false', help='Ignore alpha')
parser.add_argument('--use_exact_alpha', '-e', dest='exact_alpha', action='store_true', help='Use exact alpha')
parser.add_argument('--debug', '-d', dest='debug', action='store_true', help='Debug')
parser.add_argument('--background', '-b', type=lambda hexCode : tuple([int(hexCode.lstrip('#')[i:i+2], 16) for i in (0, 2, 4)]), default='000000', help='Background color in rgb hex')
parser.add_argument('images', nargs='*', default=[], help='path to images')

a = parser.parse_args()

if a.debug:
    print(a)

for path in a.images:
    img = Image.open(path, 'r')

    mask = deepcopy(img)
    mask_p = mask.load()

    bg = Image.new('RGB', img.size, a.background)
    bg.paste(img, None, img)
    img = bg
    
    for y in range(img.size[1]):
        for x in range(img.size[0]):
            if not a.alpha:
                mask_p[x, y] = (*mask_p[x, y][:3], 255)
            elif not a.exact_alpha:
                mask_p[x, y] = (*mask_p[x, y][:3], (mask_p[x, y][3] // 255) * 255)
            else:
                mask_p[x, y] = (*mask_p[x, y][:3], min((mask_p[x, y][3] // 1) * 255, 255))

    if a.debug:
        #mask.show()
        img.show()
        show_img = Image.new('RGBA', img.size, (0, 0, 0, 0))
        show_img.paste(img, None, mask)
        #show_img.show()

    data = bytearray()

    img_p = img.load()

    for y in range(img.size[1]):
        for x in range(img.size[0]):
            if mask_p[x, y][3] > 0:
                msb=(img_p[x, y][0] & 0xF8) | (img_p[x, y][1] >> 5);
                lsb=((img_p[x, y][1] & 0x1C) << 3) | (img_p[x, y][2]  >> 3);
                data.extend(x.to_bytes(length=1, byteorder='big'))
                data.extend(y.to_bytes(length=1, byteorder='big'))
                data.extend(lsb.to_bytes(length=1, byteorder='big'))
                data.extend(msb.to_bytes(length=1, byteorder='big'))
    print(path[path.rfind('/') + 1:path.rfind('.')], '=', data)
