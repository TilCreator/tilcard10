import pride
import leds
import color
import display
import leds_plus
from utime import alarm, time_ms, sleep


gay_active = None
i = 0


def gay_handle(x=None):
    global i, gay_active

    if gay_active:
        pass
        #current = float(time_ms())
        #alarm(int(current / 1000 + 1), _gay_timer)
    else:
        gay_active = None

    i = (i + 0.01) % 1
    leds.set_all(leds_plus.rainbow(length=11, scale=0.3, offset=i) + leds_plus.rainbow(length=4, scale=0.5, offset=i))


def gay(leds_active=True, display_active=True, brightness=1):
    global bri, gay_active

    bri = brightness

    #if gay_active is True:
    #    gay_active = False
    #    while gay_active is not None:
    #        sleep(0.01)
    gay_active = True

    if display_active:
        pride.show_display()

        with display.open() as d:
            d.backlight(int(bri / 8 * 100))

    if leds_active:
        leds.dim_top(bri)
        leds.dim_bottom(bri)

        leds.set_rocket(0, bri)
        leds.set_rocket(1, bri)
        leds.set_rocket(2, bri)

        #_gay_timer()
        gay_handle()


if __name__ == '__main__':
    gay()
    while gay_active:
        gay_handle()
        sleep(0.05)