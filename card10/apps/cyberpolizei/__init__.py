import display
import utime
import leds
from color import Color
disp = display.open()
yellow = [255, 255, 0]
black = [0, 0, 0]
red = [255, 0, 0]
blue = [0, 0, 255]
white = [255, 255, 255]
displayrot = [yellow, black]
ledrot = [blue, white, red]
dispstep = 0
ledstep = 0

while True:
    
    
    ndisp = (dispstep + 1) % len(displayrot)
    disp.clear(col=displayrot[dispstep])
    disp.print("CYBER", fg=displayrot[ndisp], bg=displayrot[dispstep], posx=80 - round(len("CYBER") / 2 * 14), posy=10)
    disp.print("CYBER", fg=displayrot[ndisp], bg=displayrot[dispstep], posx=80 - round(len("CYBER") / 2 * 14), posy=32)
    disp.print("POLIZEI", fg=displayrot[ndisp], bg=displayrot[dispstep], posx=80 - round(len("POLIZEI") / 2 * 14), posy=54)
    disp.update()
    dispstep = ndisp
    
    ledid = 11 + 2 * dispstep
    
    for i in range(3):
        # LEDs blinken lassen in aktueller ledfarbe.
        leds.set(ledid, ledrot[ledstep])
        leds.set(ledid+1, ledrot[ledstep])
        for i in range(5):
            leds.set(i*2 + dispstep, ledrot[ledstep]) 
        leds.update()
        utime.sleep_ms(100)
        leds.set(ledid, black)
        leds.set(ledid+1, black)
        for i in range(6):
            leds.set(i*2 + dispstep, black)
        leds.update()
        utime.sleep_ms(10)    
    ledstep = (ledstep + 1) % len(ledrot)
    
    
