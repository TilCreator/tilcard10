from color import Color
import display
import utime

HCOLORS = {
    1: Color.from_hex(0xf44336),
    2: Color.from_hex(0xc62828),
    3: Color.from_hex(0xef9a9a),
    4: Color.from_hex(0xe53935),
    5: Color.from_hex(0xc62828),
    6: Color.from_hex(0xb71c1c)
}
HPIXELS = [
    [0, 1, 1, 0, 2, 2, 0],
    [1, 3, 1, 1, 4, 5, 6],
    [1, 1, 1, 4, 2, 2, 6],
    [0, 1, 4, 5, 6, 6, 0],
    [0, 0, 6, 6, 6, 0, 0],
    [0, 0, 0, 6, 0, 0, 0]
]


def scale(times):
    result = []
    for row in HPIXELS:
        cl = []
        for p in row:
            for i in range(times):
                cl.append(p)
        for i in range(times):
            result.append(cl)
    return result


SIZES = dict((i, scale(i)) for i in range(8))


def draw_heart(X, Y, size):
    with display.open() as disp:
        disp.clear()
        for y, row in enumerate(SIZES[size]):
            for x, p in enumerate(row):
                if p:
                    disp.pixel(X+x, Y+y, col=HCOLORS[p])
        disp.update()


def pulse():
    i = 0
    di = 1
    while True:
        draw_heart(80-i*3, 40-i*3, i)
        if i == 7:
            di = -1
        if i == 0:
            di = 1
        i = (i + di)
        if i > 4:
            utime.sleep(0.1)
        else:
            utime.sleep(0.3)


pulse()
