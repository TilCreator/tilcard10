import display
import buttons
from test_pictures import pictures
from utime import time_ms


def wait():
    while True:
        pressed = buttons.read(buttons.TOP_RIGHT)
        if pressed != 0:
            while True:
                pressed = buttons.read(buttons.TOP_RIGHT)
                if pressed == 0:
                    break
            break

if __name__ == '__main__':
    while True:
        with display.open() as d:
            d.clear()

            for name, picture in {'alpha_test': pictures.gradient, 'tckab': pictures.tckamb}.items():
                if name == 'tckab':
                    overlays = [False, True]
                else:
                    overlays = [False]
                for overlay in overlays:
                    if overlay:
                        offset_x = 40
                    else:
                        offset_x = 0

                    d.clear()
                    d.update()

                    if overlay:
                        print('show_underlay(...)')
                        d.pixels(pictures.gradient)

                    print('d.pixels("{}", offset_x={}, offset_y=0)'.format(name, offset_x))

                    start_time = time_ms()

                    d.pixels(picture, offset_x=offset_x, offset_y=0)
                    d.update()

                    print('time:', float(time_ms() - start_time) / 1000, 'sec\n')

                    wait()
