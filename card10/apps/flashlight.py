import display
import leds
import color

bri = 8

leds.dim_top(bri)
leds.dim_bottom(bri)

leds.set_rocket(0, bri)
leds.set_rocket(1, bri)
leds.set_rocket(2, bri)

leds.set_all([color.WHITE] * 15)

with display.open() as d:
    d.rect(0, 0, 160, 80, col=color.WHITE)
    d.update()
