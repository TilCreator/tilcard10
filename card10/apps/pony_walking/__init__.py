import display
from utime import time_ms, sleep_ms

from pony_walking.derpy_frames import frames


with display.open() as d:
    i = 0
    width = display.get_pixels_size(frames[0])[4]
    while True:
        for frame in frames:
            start_time = time_ms()

            d.clear()
            d.pixels(frame, offset_x=i - width)
            d.update()

            sleep_ms(200 - (time_ms() - start_time))

            i = (i + 8) % (160 + 20 + width)
