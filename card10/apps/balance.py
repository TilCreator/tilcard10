import utime
import display
import leds
import ledfx
import buttons
import light_sensor
import ujson
import os
import bhi160

disp = display.open()
sensor = 0

pos_led=6

sensors = [
    {"sensor": bhi160.BHI160Orientation(), "name": "Orientation"},
    {"sensor": bhi160.BHI160Accelerometer(), "name": "Accelerometer"},
    {"sensor": bhi160.BHI160Gyroscope(), "name": "Gyroscope"},
]

def render_dot(pos, color):
    # WIP, hf
    if pos < 0:
        pos = 0
    if pos > 10:
        pos = 10
    
    leds.clear()
    leds.set(pos, color) 
    leds.dim_top(3)

while True:
    samples = sensors[sensor]["sensor"].read()
    if len(samples) > 0:
        disp.clear()
        sample = samples[0]

        color = [255, 0, 0]
        if sample.status == 1:
            color = [255, 128, 0]
        elif sample.status == 2:
            color = [255, 255, 0]
        elif sample.status == 3:
            color = [0, 200, 0]

        pos_led = sample.z + pos_led
        if pos_led < -2:
            pos_led = -2
        if pos_led > 13:
            pos_led = 13
        render_dot(int(pos_led), color)

        disp.print(sensors[sensor]["name"], posy=0)
        disp.print("X: %f" % sample.x, posy=20, fg=color)
        disp.print("Y: %f" % sample.y, posy=40, fg=color)
        disp.print("Z: %f" % sample.z, posy=60, fg=color)

        disp.update()

    v = buttons.read(buttons.BOTTOM_RIGHT)
    if v == 0:
        button_pressed = False

    if not button_pressed and v & buttons.BOTTOM_RIGHT != 0:
        button_pressed = True
        sensor = (sensor + 1) % len(sensors)
