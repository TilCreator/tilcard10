import display, utime, math

model = [
((-1.1, 1.0,-1.0),( 1.0, 1.0,-1.0)),
(( 1.0, 1.0,-1.0),( 1.0, 1.0, 1.0)),
(( 1.0, 1.0, 1.0),(-1.0, 1.0, 1.0)),
((-1.0, 1.0, 1.0),(-1.0, 1.0,-1.0)),

((-1.0,-1.0,-1.0),( 1.0,-1.0,-1.0)),
(( 1.0,-1.0,-1.0),( 1.0,-1.0, 1.0)),
(( 1.0,-1.0, 1.0),(-1.0,-1.0, 1.0)),
((-1.0,-1.0, 1.0),(-1.0,-1.0,-1.0)),

((-1.0, 1.0,-1.0),(-1.0,-1.0,-1.0)),
(( 1.0, 1.0,-1.0),( 1.0,-1.0,-1.0)),
(( 1.0, 1.0, 1.0),( 1.0,-1.0, 1.0)),
((-1.0, 1.0, 1.0),(-1.0,-1.0, 1.0))]

def to_screen_coord(coord):
  z = (coord[2]+3.0)/20.0
  x = coord[0]/z
  y = coord[1]/z
  screenx = int(x)+80
  screeny = int(y)+40
  return (screenx,screeny)

def translate(coord, t):
  return (coord[0]+t[0], coord[1]+t[1], coord[2]+t[2])

def rotatey(coord, t):
  s = math.sin(t)
  c = math.cos(t)
  return (coord[0]*c-coord[2]*s, coord[1], coord[0]*s+coord[2]*c)

with display.open() as d:
  start = utime.time_ms()
  while True:
    d.clear()
    for line in model:
      time = float(utime.time_ms()-start)/1000.0
      offset = math.sin(time)+0.5
      c1 = to_screen_coord(rotatey(translate(line[0], (0.0,offset,0.0)), time))
      c2 = to_screen_coord(rotatey(translate(line[1], (0.0,offset,0.0)), time))
      d.line(c1[0], c1[1], c2[0], c2[1], col=(255,255,255))
    d.update()