import utime
import bme680
import display
import light_sensor
import leds
import pride
import buttons
import personal_state
import os
import vibra

SLEEP_TIME  = .1
UPDATE_DIVIDER = 5
DOW = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su']
PSTATE = personal_state.COMMUNICATION
N_STATES = 5
START_STATE = 4
VIBRA_DIVIDER = 100
BATTERY_LIMIT = 3.6
VIBRA_TIME = 60

def toggle_pride(pride_state):
    if pride_state:
        leds.clear()
        pride_state = False
    else:
        pride.show_leds()
        pride_state = True

    return pride_state

button_pressed = False
counter = 0
pride_state = True
vibra_counter = 0

bme680.init()
light_sensor.start()
blink_state = False
state = START_STATE

pride.show_leds()
personal_state.set(PSTATE, True)


with display.open() as disp:
    while True:
        # Change state on button press
        pressed = buttons.read(buttons.BOTTOM_LEFT | buttons.BOTTOM_RIGHT |buttons.TOP_RIGHT)

        if pressed == 0:
            button_pressed = False

        if not button_pressed and pressed & buttons.BOTTOM_LEFT != 0:
            button_pressed = True
            state = (state - 1) % N_STATES
        elif not button_pressed and pressed & buttons.BOTTOM_RIGHT != 0:
            button_pressed = True
            state = (state + 1) % N_STATES
        elif not button_pressed and pressed & buttons.TOP_RIGHT != 0:
            button_pressed = True
            pride_state = toggle_pride(pride_state)

        # Update display
        if button_pressed or counter == 0:
            lt = utime.localtime()
            year = lt[0]
            month = lt[1]
            day = lt[2]
            hour = lt[3]
            mi = lt[4]
            sec = lt[5]
            dow = lt[6]
            temp, hum, press, _ = bme680.get_data()
            brightness = light_sensor.get_reading()
            battery = os.read_battery()

            disp.clear()
            disp.print('{:02d}:{:02d}:{:02d} {}'.format(hour, mi, sec, DOW[dow]))
            disp.print('{}-{:02d}-{:02d}'.format(year, month, day), posy=20)

            if state == 0:
                line = 'T: {:.2f}C'.format(temp)
            elif state == 1:
                line = 'RH: {:.2f}%'.format(hum)
            elif state == 2:
                line = 'p:{:.1f}hPa'.format(press)
            elif state == 3:
                line = 'B: {}'.format(brightness)
            elif state == 4:
                line = 'V: {:.2f}V'.format(battery)
            disp.print(line, posy=50)

            disp.update()

            if blink_state:
                leds.set_rocket(2, 0)
                blink_state = False
            else:
                leds.set_rocket(2, 31)
                blink_state = True

        # Vibration as low battery warning
        if vibra_counter == 0:
            battery = os.read_battery()

            if battery < BATTERY_LIMIT:
                vibra.vibrate(60)

        # Increase counters and sleep
        counter = (counter + 1) % UPDATE_DIVIDER
        vibra_counter = (vibra_counter + 1) % VIBRA_DIVIDER
        utime.sleep(SLEEP_TIME)
