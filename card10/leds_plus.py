"""
some custom leds function
"""

import leds
import color

def rainbow(length=15, offset=0, scale=1, saturation=1, value=1):
    l = []
    for i in range(length):
        l.append(color.from_hsv(int(((i / length) * scale * 360) + offset * 360) % 360, saturation, value))
    return l
